<!doctype html>
<html lang="en" class="h-100">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <!-- Berusaha untuk murni Bootstrap 😔 -->
    <style>
      /* Atur BS pl-2 & pr-2 untuk ganjil genap */

      .da-bowl .col-6:nth-child(odd) {
        padding-right: 0.5rem !important;
      }

      .da-bowl .col-6:nth-child(even) {
        padding-left: 0.5rem !important;
      }

      /* Nav aktif */
      .nav-pills .nav-link.active:not(.k8g2b2) {
        color: #6c757d !important;
      }
    </style>

    <title>k8g2b2</title>
  </head>
  <body class="h-100 bg-secondary">
    <div class="container h-100">
      <div class="row h-100 justify-content-center">
        <div class="col-md-9 col-lg-6">

          <!-- Navbar -->
          <div class="row justify-content-center">
            <div class="col mt-3">
              <nav class="nav nav-pills flex-column text-center">
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill k8g2b2 active" href="#"><b>k8g2b2</b></a>
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill active" href="#">Beranda</a>
                <!-- Kalau belum login, "Mendaftar" -->
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill" href="#">Tsubasa Ha … (22)</a>
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill" href="#"><b>Tanya?</b></a>
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill" href="#">#Tagar</a>
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill" href="#">Keluar(!)</a>
              </nav>
            </div>
          </div>
          <!-- Navbar selesai -->

          <!-- Main -->
          <div class="row justify-content-center">
            <div class="col">
              <!-- Sampel card simpel -->
              <div class="card mb-3">
                <div class="card-body">
                  <h3>
                    <a href="" class="text-reset text-decoration-none">
                      Belum ada Pertanyaan? Silakan ber-Tanya?
                    </a>
                  </h3>
                </div>
              </div>
              <div class="row text-center da-bowl">
                <div class="col-6">
                  <div class="card mb-3 rounded-pill">
                    <div class="card-body">
                      <h3 class="m-0">
                        <a href="" class="text-reset text-decoration-none">
                          Tanya?
                        </a>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Sampel card simpel selesai -->

              <!-- Profile -->
              <div class="card mb-3">
                <div class="card-body">
                  <form action="">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Nama" value="Tsubasa Hanekawa" disabled>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Email" value="tsubasahanekawa2022@outlook.com" disabled>
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control" placeholder="Umur" value="22">
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" placeholder="Biodata">Idk everything~</textarea>
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" placeholder="Alamat">Al-Quds</textarea>
                    </div>
                    <button class="btn btn-secondary btn-block mt-3" type="submit">Edit</button>
                  </form>
                </div>
              </div>

              <div class="card mb-3">
                <div class="card-body">
                  <a href="#" class="badge badge-pill badge-secondary mb-2">#Buku</a>
                  <!-- <p class="mb-2">
                    <b>
                      <a href="" class="text-reset text-decoration-none">
                        Anna Karina
                      </a>
                    </b>
                  </p> -->
                  <h3>
                    <a href="" class="text-reset text-decoration-none">
                      Asalamualaikum. Rekomendasi novel pendek?
                    </a>
                  </h3>
                  <!-- <a href="">
                    <div class="mb-2">
                      <img src="https://pbs.twimg.com/media/E4VBVZOUUAYrpxX?format=jpg&name=large" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
                    </div>
                  </a> -->
                  <a href="" class="text-muted text-decoration-none d-block float-left mt-1">
                    244 respons~
                  </a>
                  <div class="btn-group float-right mt-1">
                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Edit</a>
                      <a class="dropdown-item" href="#">Hapus</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card mb-3">
                <div class="card-body">
                  <a href="#" class="badge badge-pill badge-secondary mb-2">#Film</a>
                  <h3>
                    <a href="" class="text-reset text-decoration-none">
                      Ini film apa?
                    </a>
                  </h3>
                  <a href="">
                    <div class="mb-2">
                      <img src="https://pbs.twimg.com/media/E4TY3-nVIAIVNwG?format=jpg&name=large" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
                    </div>
                  </a>
                  <a href="" class="text-muted text-decoration-none d-block float-left mt-1">
                    621 respons~
                  </a>
                  <div class="btn-group float-right mt-1">
                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Edit</a>
                      <a class="dropdown-item" href="#">Hapus</a>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Profile selesai -->

              <!-- Tanya -->
              <div class="card mb-3">
                <div class="card-body">
                  <form action="">
                    <div class="form-group">
                      <textarea id="inputTanya" class="form-control" placeholder="Pertanyaan"></textarea>
                    </div>
                    <div class="form-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Foto?</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <select class="form-control custom-select" id="inputTagar">
                        <option value="" disabled selected hidden>#Tagar</option>
                        <option value="1">#Anime</option>
                        <option value="2">#Buku</option>
                        <option value="3">#Film</option>
                        <option value="4">#Musik</option>
                        <option value="5">#VTuber</option>
                      </select>
                    </div>
                    <button class="btn btn-secondary btn-block mt-3" type="submit">Tanya?</button>
                  </form>
                </div>
              </div>
              <!-- Tanya selesai -->

              <!-- Tagar -->
              <div class="row text-center da-bowl">
                <div class="col-6">
                  <div class="card mb-3">
                    <div class="card-body">
                      <h3 class="m-0">
                        <a href="" class="text-reset text-decoration-none">
                          #Anime
                        </a>
                      </h3>
                      <div class="btn-group mx-auto mt-2">
                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Edit</a>
                          <a class="dropdown-item" href="#">Hapus</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-6">
                  <div class="card mb-3">
                    <div class="card-body">
                      <h3 class="m-0">
                        <a href="" class="text-reset text-decoration-none">
                          #Buku
                        </a>
                      </h3>
                      <div class="btn-group mx-auto mt-2">
                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Edit</a>
                          <a class="dropdown-item" href="#">Hapus</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-6">
                  <div class="card mb-3">
                    <div class="card-body">
                      <h3 class="m-0">
                        <a href="" class="text-reset text-decoration-none">
                          #Film
                        </a>
                      </h3>
                      <div class="btn-group mx-auto mt-2">
                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Edit</a>
                          <a class="dropdown-item" href="#">Hapus</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-6">
                  <div class="card mb-3">
                    <div class="card-body">
                      <h3 class="m-0">
                        <a href="" class="text-reset text-decoration-none">
                          #Musik
                        </a>
                      </h3>
                      <div class="btn-group mx-auto mt-2">
                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Edit</a>
                          <a class="dropdown-item" href="#">Hapus</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-6">
                  <div class="card mb-3">
                    <div class="card-body">
                      <h3 class="m-0">
                        <a href="" class="text-reset text-decoration-none">
                          #VTuber
                        </a>
                      </h3>
                      <div class="btn-group mx-auto mt-2">
                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Edit</a>
                          <a class="dropdown-item" href="#">Hapus</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-6">
                  <div class="card mb-3 rounded-pill">
                    <div class="card-body">
                      <h3 class="m-0">
                        <a href="" class="text-reset text-decoration-none">
                          Tambah #
                        </a>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Tagar selesai -->

              <!-- Beranda -->
              <div class="card mb-3">
                <div class="card-body">
                  <a href="#" class="badge badge-pill badge-secondary mb-2">#Film</a>
                  <p class="mb-2">
                    <b>
                      <a href="" class="text-reset text-decoration-none">
                        Hong Sang-soo
                      </a>
                    </b>
                  </p>
                  <h3>
                    <a href="" class="text-reset text-decoration-none">
                      Film yang bagus ditonton untuk menghindari futur?
                    </a>
                  </h3>
                  <a href="" class="text-muted text-decoration-none d-block float-left mt-1">
                    14 respons~
                  </a>
                </div>
              </div>

              <div class="card mb-3">
                <div class="card-body">
                  <a href="#" class="badge badge-pill badge-secondary mb-2">#Buku</a>
                  <p class="mb-2">
                    <b>
                      <a href="" class="text-reset text-decoration-none">
                        Anna Karina
                      </a>
                    </b>
                  </p>
                  <h3>
                    <a href="" class="text-reset text-decoration-none">
                      Rekomen novel NISIOISIN habis namatin (catch-up) seri MONOGATARI, dong?
                    </a>
                  </h3>
                  <a href="">
                    <div class="mb-2">
                      <img src="https://pbs.twimg.com/media/E4VBVZOUUAYrpxX?format=jpg&name=large" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
                    </div>
                  </a>
                  <a href="" class="text-muted text-decoration-none d-block float-left mt-1">
                    2 respons~
                  </a>
                </div>
              </div>
              <!-- Beranda selesai -->

              <!-- Detail pertanyaan -->
              <!-- Pertanyaan -->
              <div class="card mb-3">
                <div class="card-body">
                  <a href="#" class="badge badge-pill badge-secondary mb-2">#Musik</a>
                  <p class="mb-2">
                    <b>
                      <a href="" class="text-reset text-decoration-none">
                        Tsubasa Hanekawa
                      </a>
                    </b>
                  </p>
                  <h3>
                    <!-- <a href="" class="text-reset text-decoration-none"> -->
                      Artis KAMITSUBAKI yang enak selain KAF? (Terutama member V.W.P …)
                    <!-- </a> -->
                  </h3>
                  <div class="mb-2">
                    <img src="https://img.youtube.com/vi/M1RIUrgJqWw/maxresdefault.jpg" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
                  </div>
                  <div class="btn-group float-right mt-1">
                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Edit</a>
                      <a class="dropdown-item" href="#">Hapus</a>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Pertanyaan selesai -->

              <!-- Jawaban -->
              <div class="card mb-3">
                <div class="card-body">
                  <p class="mb-2">
                    <b>
                      <a href="" class="text-reset text-decoration-none">
                        Senjougahara
                      </a>
                    </b>
                  </p>
                  <p class="mb-2">
                    <a href="" class="text-reset text-decoration-none">
                      Gue suka Isekaijoucho sama RIM
                    </a>
                  </p>
                  <!-- <div class="btn-group float-right mt-1">
                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Edit</a>
                      <a class="dropdown-item" href="#">Hapus</a>
                    </div>
                  </div> -->
                </div>
              </div>

              <div class="card mb-3">
                <div class="card-body">
                  <p class="mb-2">
                    <b>
                      <a href="" class="text-reset text-decoration-none">
                        Budi Sudarsono
                      </a>
                    </b>
                  </p>
                  <p class="mb-2">
                    <a href="" class="text-reset text-decoration-none">
                      THIS!!!!!!1!!!111
                    </a>
                  </p>
                  <div class="mb-2">
                    <img src="https://yt3.ggpht.com/63ZbuotELvSvx4QOY6S3Z5s1YrUabTL1olr3NT48l3kY3iHymxSdX_t8Ecue7qzCW76VcnhX=s900-c-k-c0x00ffffff-no-rj" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
                  </div>
                  <div class="btn-group float-right mt-1">
                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Edit</a>
                      <a class="dropdown-item" href="#">Hapus</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card mb-3">
                <div class="card-body">
                  <p class="mb-2">
                    <b>
                      <a href="" class="text-reset text-decoration-none">
                        Ougi Oshino
                      </a>
                    </b>
                  </p>
                  <p class="mb-2">
                    <a href="" class="text-reset text-decoration-none">
                      RIM kesannya lebih dewasa kalau dibandingin sama KAF, menurutku, sih … RIM kesannya lebih dewasa kalau dibandingin sama KAF, menurutku … RIM kesannya lebih dewasa kalau ke sananya …
                    </a>
                  </p>
                  <div class="btn-group float-right mt-1">
                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Edit</a>
                      <a class="dropdown-item" href="#">Hapus</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card mb-3">
                <div class="card-body">
                  <form action="">
                    <div class="form-group">
                      <textarea id="inputJawaban" class="form-control" placeholder="Jawaban"></textarea>
                    </div>
                    <div class="form-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Foto?</label>
                      </div>
                    </div>
                    <button class="btn btn-secondary btn-block mt-3" type="submit">Jawab~</button>
                  </form>
                </div>
              </div>
              <!-- Jawaban selesai -->
              <!-- Detail pertanyaan selesai -->

            </div>
          </div>
          <!-- Main selesai -->

          <!-- Footer -->
          <div class="row justify-content-center">
            <!-- <p class="text-black-50">
              <a class="text-reset text-decoration-none" href="https://gitlab.com/k8g2b2">
                <small>© 2022 Kelompok 8, Grup 2, Batch 2</small>
              </a>
            </p> -->
            <div class="col">
              <nav class="nav nav-pills flex-column text-center">
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill" href="https://gitlab.com/k8g2b2">
                  <small class="text-muted">© 2022 Kelompok 8, Grup 2, Batch 2</small>
                </a>
              </nav>
            </div>
          </div>
          <!-- Footer selesai -->

        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
    -->

    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
    <script>
      $(document).ready(function () {
        bsCustomFileInput.init()
      })
    </script>
  </body>
</html>
