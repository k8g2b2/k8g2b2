# k8g2b2 (Final Project)

k8g2b2 adalah sebuah proyek bertema forum tanya jawab oleh Kelompok 8, Grup 2, Batch 2 dari kelas Full Stack Web Development, PKS Digital School, 2022.

## Kelompok 8 (Grup 2, Batch 2)

### Anggota Kelompok

- Muhammad Alif
<!-- - Rahmat Satyawan -->
- Ananda Muhammad Fitrah

## Tema Project

Forum Tanya Jawab

## ERD

![ERD k8g2b2](public/k8g2b2-erd.png)

## Link

- Demo: https://www.youtube.com/watch?v=wmC0RP3cnAg
- Heroku: https://k8g2b2.herokuapp.com/
