<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PertanyaanController@index');
Route::post('/', 'PertanyaanController@store');

Route::resource('pertanyaan', 'PertanyaanController')->except([
    'index', 'store'
]);

Route::redirect('pertanyaan', '/', 301);
Route::redirect('jawaban', '/', 301);

Route::resource('kategori', 'KategoriController');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('pertanyaan.jawaban', 'JawabanController')->only([
        'store', 'edit', 'update', 'destroy'
    ])->shallow();
});

Auth::routes();
