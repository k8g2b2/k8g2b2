<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = 'jawaban';
    protected $fillable = ['teks', 'foto', 'user_id', 'pertanyaan_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function pertanyaan() {
        return $this->belongsTo('App\Pertanyaan');
    }
}
