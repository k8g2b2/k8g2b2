<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['nama'];

    public function pertanyaan() {
        return $this->hasMany('App\Pertanyaan')->orderBy('id');
    }
}
