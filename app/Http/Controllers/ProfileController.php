<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;

class ProfileController extends Controller
{
    public function index() {
        $profile = Profile::where('user_id', Auth::id())->first();

        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'umur' => 'required|max:11',
            'biodata' => 'required',
            'alamat' => 'required',
        ]);

        $profile = Profile::find($id);

        $profile->umur = $request['umur'];
        $profile->biodata = $request['biodata'];
        $profile->alamat = $request['alamat'];

        $profile->save();

        alert()->success('Nice');

        return redirect('/profile');
    }
}
