<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use File;
use App\Pertanyaan;
use App\Kategori;
use JD\Cloudder\Facades\Cloudder;
// use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pertanyaan = Pertanyaan::all()->sortBy('id');

        // return view('pertanyaan.index', compact('pertanyaan'));
        return view('beranda', compact('pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();

        return view('pertanyaan.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategori = DB::table('kategori')->get();

        if (!$kategori->contains('id', $request->kategori_id)) {
            $request->merge([
                'kategori_id' => str_replace('#', '', $request->kategori_id),
            ]);

            $request->validate([
                'kategori_id' => 'required|max:45|unique:App\Kategori,nama',
            ]);

            $kategori = new Kategori;

            $kategori->nama = $request->kategori_id;

            $kategori->save();

            $kategoriId = Kategori::where('nama', $request->kategori_id)->first()->id;
        } else {
            $kategoriId = $request->kategori_id;
        }

        $request->validate([
            'teks' => 'required|max:300',
            'kategori_id' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg|max:512|dimensions:max_height=1920,min_width=320',
        ]);

        $pertanyaan = new Pertanyaan;

        if ($request->has('foto')) {
            // $fotoName = time() . '.' . $request->foto->extension();

            // $request->foto->move(public_path('foto'), $fotoName);

            $fotoName = $request->file('foto')->getRealPath();

            Cloudder::upload($fotoName, null, [
                'folder' => 'foto',
                'overwrite' => false,
                'resource_type' => 'image',
                'responsive' => true,
                'transformation' => [
                    'quality' => '66',
                ],
            ]);

            $fotoPublicId = Cloudder::getPublicId();

            $fotoUrl = Cloudder::secureShow($fotoPublicId, [
                'width' => 'iw',
                'height' => 'ih',
            ]);

            $pertanyaan->user_id = Auth::id();
            $pertanyaan->teks = $request->teks;
            // $pertanyaan->kategori_id = $request->kategori_id;
            $pertanyaan->kategori_id = $kategoriId;
            // $pertanyaan->foto = $fotoName;
            $pertanyaan->foto_public_id = $fotoPublicId;
            $pertanyaan->foto_url = $fotoUrl;
        } else {
            $pertanyaan->user_id = Auth::id();
            $pertanyaan->teks = $request->teks;
            // $pertanyaan->kategori_id = $request->kategori_id;
            $pertanyaan->kategori_id = $kategoriId;
        }

        $pertanyaan->save();

        alert()->success('GG');

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pertanyaan = Pertanyaan::findOrFail($id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pertanyaan = Pertanyaan::findOrFail($id);
        $kategori = DB::table('kategori')->get();

        return view('pertanyaan.edit', compact('pertanyaan', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategori = DB::table('kategori')->get();

        if (!$kategori->contains('id', $request->kategori_id)) {
            $request->merge([
                'kategori_id' => str_replace('#', '', $request->kategori_id),
            ]);

            $request->validate([
                'kategori_id' => 'required|max:45|unique:App\Kategori,nama',
            ]);

            $kategori = new Kategori;

            $kategori->nama = $request->kategori_id;

            $kategori->save();

            $kategoriId = Kategori::where('nama', $request->kategori_id)->first()->id;
        } else {
            $kategoriId = $request->kategori_id;
        }

        $request->validate([
            'teks' => 'required|max:300',
            'kategori_id' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg|max:512|dimensions:max_height=1920,min_width=320',
        ]);

        $pertanyaan = Pertanyaan::find($id);

        if ($request->has('foto')) {
            if ($pertanyaan->foto) {
                $path = 'foto/';

                File::delete($path . $pertanyaan->foto);
            }

            if ($pertanyaan->foto_public_id) {
                Cloudder::delete($pertanyaan->foto_public_id);
            }

            // $fotoName = time() . '.' . $request->foto->extension();

            // $request->foto->move(public_path('foto'), $fotoName);

            $fotoName = $request->file('foto')->getRealPath();

            Cloudder::upload($fotoName, null, [
                'folder' => 'foto',
                'overwrite' => false,
                'resource_type' => 'image',
                'responsive' => true,
                'transformation' => [
                    'quality' => '66',
                ],
            ]);

            $fotoPublicId = Cloudder::getPublicId();

            $fotoUrl = Cloudder::secureShow($fotoPublicId, [
                'width' => 'iw',
                'height' => 'ih',
            ]);

            $pertanyaan->teks = $request->teks;
            // $pertanyaan->kategori_id = $request->kategori_id;
            $pertanyaan->kategori_id = $kategoriId;
            // $pertanyaan->foto = $fotoName;
            $pertanyaan->foto_public_id = $fotoPublicId;
            $pertanyaan->foto_url = $fotoUrl;
        } else {
            $pertanyaan->teks = $request->teks;
            // $pertanyaan->kategori_id = $request->kategori_id;
            $pertanyaan->kategori_id = $kategoriId;
        }

        $pertanyaan->update();

        alert()->success('Nice');

        // return redirect('/pertanyaan');
        // return redirect('/');
        return redirect('/pertanyaan/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pertanyaan = Pertanyaan::find($id);

        if ($pertanyaan->foto) {
            $path = 'foto/';

            File::delete($path . $pertanyaan->foto);
        }

        if ($pertanyaan->foto_public_id) {
            Cloudder::delete($pertanyaan->foto_public_id);
        }

        $pertanyaan->jawaban()->delete();
        $pertanyaan->delete();

        alert()->success('Okeee');

        // return redirect('/pertanyaan');
        return redirect('/');
    }
}
