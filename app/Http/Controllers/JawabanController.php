<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Jawaban;
use File;
use JD\Cloudder\Facades\Cloudder;

class JawabanController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'teks' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg|max:512|dimensions:max_height=1920,min_width=320',
            'pertanyaan_id' => 'required',
        ]);

        $jawaban = new Jawaban;

        if ($request->has('foto')) {
            // $fotoName = time() . '.' . $request->foto->extension();

            // $request->foto->move(public_path('foto'), $fotoName);

            $fotoName = $request->file('foto')->getRealPath();

            Cloudder::upload($fotoName, null, [
                'folder' => 'foto',
                'overwrite' => false,
                'resource_type' => 'image',
                'responsive' => true,
                'transformation' => [
                    'quality' => '66',
                ],
            ]);

            $fotoPublicId = Cloudder::getPublicId();

            $fotoUrl = Cloudder::secureShow($fotoPublicId, [
                'width' => 'iw',
                'height' => 'ih',
            ]);

            $jawaban->teks = $request->teks;
            // $jawaban->foto = $fotoName;
            $jawaban->foto_public_id = $fotoPublicId;
            $jawaban->foto_url = $fotoUrl;
            $jawaban->user_id = Auth::id();
            $jawaban->pertanyaan_id = $request->pertanyaan_id;
        } else {
            $jawaban->teks = $request->teks;
            $jawaban->user_id = Auth::id();
            $jawaban->pertanyaan_id = $request->pertanyaan_id;
        }

        $jawaban->save();

        alert()->success('GG');

        return redirect()->back();
    }

    public function edit($id)
    {
        $jawaban = Jawaban::find($id);

        return view('jawaban.edit', compact('jawaban'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'teks' => 'required',
            'foto' => [
                'image',
                'mimes:jpeg,png,jpg',
                'max:512',
                'dimensions:max_height=1920,min_width=320',
            ],
        ]);

        $jawaban = Jawaban::find($id);

        if ($request->has('foto')) {
            if ($jawaban->foto) {
                $path = 'foto/';

                File::delete($path . $jawaban->foto);
            }

            if ($jawaban->foto_public_id) {
                Cloudder::delete($jawaban->foto_public_id);
            }

            // $fotoName = time() . '.' . $request->foto->extension();

            // $request->foto->move(public_path('foto'), $fotoName);

            $fotoName = $request->file('foto')->getRealPath();

            Cloudder::upload($fotoName, null, [
                'folder' => 'foto',
                'overwrite' => false,
                'resource_type' => 'image',
                'responsive' => true,
                'transformation' => [
                    'quality' => '66',
                ],
            ]);

            $fotoPublicId = Cloudder::getPublicId();

            $fotoUrl = Cloudder::secureShow($fotoPublicId, [
                'width' => 'iw',
                'height' => 'ih',
            ]);

            $jawaban->teks = $request->teks;
            // $jawaban->foto = $fotoName;
            $jawaban->foto_public_id = $fotoPublicId;
            $jawaban->foto_url = $fotoUrl;
        } else {
            $jawaban->teks = $request->teks;
        }

        $jawaban->update();

        alert()->success('Nice');

        return redirect('/pertanyaan/' . $jawaban->pertanyaan_id);
    }

    public function destroy($id)
    {
        $jawaban = Jawaban::find($id);

        if ($jawaban->foto) {
            $path = 'foto/';

            File::delete($path . $jawaban->foto);
        }

        if ($jawaban->foto_public_id) {
            Cloudder::delete($jawaban->foto_public_id);
        }

        $jawaban->delete();

        alert()->success('Okeee');

        return redirect('/pertanyaan/' . $jawaban->pertanyaan_id);
    }
}
