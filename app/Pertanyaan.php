<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = 'pertanyaan';
    protected $fillable = ['teks', 'foto', 'user_id', 'kategori_id'];

    public function kategori() {
        return $this->belongsTo('App\Kategori');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function jawaban() {
        return $this->hasMany('App\Jawaban')->oldest();
    }
}
