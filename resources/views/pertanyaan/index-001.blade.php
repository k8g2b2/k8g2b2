@extends('layouts.master')

@section('title')
  @auth
    Beranda @
  @endauth
@endsection

@section('content')
  @forelse ($pertanyaan->reverse() as $item)
    <div class="card mb-3">
      <div class="card-body">
        <a href="/kategori/{{$item->kategori->id}}" class="badge badge-pill badge-secondary mb-2">
          #{{$item->kategori->nama}}
        </a>
        <p class="mb-2">
          <b>
            {{-- <a href="/profile/{{$item->user->id}}" class="text-reset text-decoration-none">
              {{$item->user->profile->nama}}
            </a> --}}
          </b>
        </p>
        <h3>
          <a href="/pertanyaan/{{$item->id}}" class="text-reset text-decoration-none">
            {{$item->teks}}
          </a>
        </h3>

        @if ($item->foto != '')
          <a href="/pertanyaan/{{$item->id}}">
            <img src="{{asset('foto/' . $item->foto)}}" alt="" class="img-fluid rounded mt-2 mb-2 w-100">
          </a>
        @endif

        <small class="text-muted">
          <b>
            <a href="/pertanyaan/{{$item->id}}" class="text-reset text-decoration-none">
              2 jawaban
            </a>
          </b>
        </small>
        <div class="btn-group float-right mt-2">
          <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="/pertanyaan/{{$item->id}}/edit">Edit</a>
            <a class="dropdown-item" href="/pertanyaan/{{ $item->id }}"
                onclick="event.preventDefault();
                    document.getElementById('hapus-form-{{ $item->id }}').submit();">Hapus</a>
            <form id="hapus-form-{{ $item->id }}" action="/pertanyaan/{{ $item->id }}" method="post" style="display: none;">
              @csrf
              @method('delete')
            </form>
          </div>
        </div>
      </div>
    </div>
  @empty
    <div class="card mb-3">
      <div class="card-body">
        <h3>
          <a href="/pertanyaan/create" class="text-reset text-decoration-none">
            Belum ada Pertanyaan. Silakan ber-Tanya?
          </a>
        </h3>
      </div>
    </div>

    <div class="row text-center da-bowl">
      <div class="col-6">
        <div class="card mb-3 rounded-pill">
          <div class="card-body">
            <h3 class="m-0">
              <a href="/pertanyaan/create" class="text-reset text-decoration-none">
                Tanya?
              </a>
            </h3>
          </div>
        </div>
      </div>
    </div>
  @endforelse
@endsection
