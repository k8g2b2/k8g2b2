@extends('layouts.master')

@push('script')
  <script src="https://cdn.tiny.cloud/1/vijhpxy7k856u1i7orvl3k7vi1247uhaq38pvw0gaaue114w/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

  <script>
    tinymce.init({
      selector: 'textarea.tinymce',
      menubar: 'format',
      forced_root_block : false,
      plugins: 'autolink paste',
      default_link_target: '_blank',
      // paste_as_text: true,
      invalid_elements: 'img',
    });
  </script>
@endpush

@section('title')
  {{ $pertanyaan->teks }} @
@endsection

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <a href="/kategori/{{$pertanyaan->kategori->id}}" class="badge badge-pill badge-secondary mb-2">
        #{{$pertanyaan->kategori->nama}}
      </a>
      <p class="mb-2">
        <b>
          {{$pertanyaan->user->name}}
        </b>
      </p>
      <h3>
        {{-- <a href="/pertanyaan/{{$pertanyaan->id}}" class="text-reset text-decoration-none"> --}}
          {{-- {{ $pertanyaan->teks }} --}}
          <?= nl2br($pertanyaan->teks) ?>
        {{-- </a> --}}
      </h3>

      @if ($pertanyaan->foto)
        {{-- <a href="/pertanyaan/{{$pertanyaan->id}}"> --}}
          <div class="mb-2">
            <img src="{{asset('foto/' . $pertanyaan->foto)}}" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
          </div>
        {{-- </a> --}}
      @elseif ($pertanyaan->foto_url)
        <div class="mb-2">
          <img src="{{ $pertanyaan->foto_url }}" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
        </div>
      @endif

      {{-- <a href="/pertanyaan/{{$pertanyaan->id}}" class="text-muted text-decoration-none d-block float-left mt-1"> --}}
      <a class="text-muted text-decoration-none d-block float-left mt-1">
        @if ($pertanyaan->jawaban->count() != 0)
          {{ $pertanyaan->jawaban->count() }} respons~
        @else
          Belum ada respons …
        @endif
      </a>

      @auth
        @if (Auth::id() == $pertanyaan->user->id)
          <div class="btn-group float-right mt-1">
            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="/pertanyaan/{{$pertanyaan->id}}/edit">Edit</a>
              <a class="dropdown-item" href="/pertanyaan/{{ $pertanyaan->id }}"
                  onclick="event.preventDefault();
                      document.getElementById('hapus-form-{{ $pertanyaan->id }}').submit();">Hapus</a>
              <form id="hapus-form-{{ $pertanyaan->id }}" action="/pertanyaan/{{ $pertanyaan->id }}" method="post" style="display: none;">
                @csrf
                @method('delete')
              </form>
            </div>
          </div>
        @endif
      @endauth

    </div>
  </div>

  @foreach ($pertanyaan->jawaban as $item)
    <div class="card mb-3">
      <div class="card-body">
        <p class="mb-2">
          <b>
            {{ $item->user->name }}
          </b>
        </p>
        {{-- <p class="mb-2"> --}}
        <div class="mb-2">
          {{-- {!! nl2br(e($item->teks)) !!} --}}
          {!! $item->teks !!}
        {{-- </p> --}}
        </div>

        @if ($item->foto)
          <div class="mb-2">
            <img src="{{asset('foto/' . $item->foto)}}" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
          </div>
        @elseif ($item->foto_url)
          <div class="mb-2">
            <img src="{{ $item->foto_url }}" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
          </div>
        @endif

        @auth
          @if (Auth::id() == $item->user->id)
            <div class="btn-group float-right mt-1">
              <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="/jawaban/{{ $item->id }}/edit">Edit</a>
                <a class="dropdown-item" href="/jawaban/{{ $item->id }}"
                    onclick="event.preventDefault();
                        document.getElementById('hapus-form-{{ $item->id }}').submit();">Hapus</a>
                <form id="hapus-form-{{ $item->id }}" action="/jawaban/{{ $item->id }}" method="post" style="display: none;">
                  @csrf
                  @method('delete')
                </form>
              </div>
            </div>
          @endif
        @endauth

      </div>
    </div>
  @endforeach

  <div class="card mb-3">
    <div class="card-body">
      @guest
        <form>
          <div class="form-group">
            <textarea class="form-control" placeholder="Respons …" disabled></textarea>
          </div>
          <div class="form-group">
            <div class="custom-file">
              <input type="file" class="custom-file-input" disabled>
              <label class="custom-file-label">Foto?</label>
            </div>
          </div>
          <a href="/login" class="btn btn-secondary btn-block mt-3">Masuk untuk merespons~</a>
        </form>
      @endguest

      @auth
        <form action="/pertanyaan/{{ $pertanyaan->id }}/jawaban" method="post" enctype="multipart/form-data">
          @csrf

          <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan->id }}">

          <div class="form-group">
            <textarea class="form-control tinymce" placeholder="Respons …" name="teks"></textarea>
          </div>
          @error('teks')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror

          <div class="form-group">
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="customFile" name="foto">
              <label class="custom-file-label" for="customFile">Foto?</label>
            </div>
          </div>
          @error('foto')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror

          <button class="btn btn-secondary btn-block mt-3" type="submit">Jawab~</button>
        </form>
      @endauth

    </div>
  </div>
@endsection
