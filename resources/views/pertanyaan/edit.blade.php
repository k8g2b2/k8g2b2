@extends('layouts.master')

@push('style')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@ttskch/select2-bootstrap4-theme@x.x.x/dist/select2-bootstrap4.min.css">

@endpush

@push('script')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script>
    $(document).ready(function() {
      $('.select2').select2({
        tags: true,
        placeholder: '#Tagar',
        theme: 'bootstrap4',
      });
    });
  </script>
@endpush

@section('title', 'Edit Pertanyaan? @ ')

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <form action="/pertanyaan/{{$pertanyaan->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="form-group">
          <textarea name="teks" class="form-control" placeholder="Pertanyaan">{{$pertanyaan->teks}}</textarea>
        </div>
        @error('teks')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <div class="custom-file">
            <input type="file" name="foto" class="custom-file-input" id="customFile">
            <label class="custom-file-label" for="customFile">Foto?</label>
          </div>
        </div>
        @error('foto')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <select name="kategori_id" class="form-control custom-select select2">
            <option value="" disabled selected hidden>#Tagar</option>

            @foreach ($kategori as $item)
              @if ($item->id == $pertanyaan->kategori_id)
                <option value="{{$item->id}}" selected>#{{$item->nama}}</option>
              @else
                <option value="{{$item->id}}">#{{$item->nama}}</option>
              @endif
            @endforeach
          </select>
        </div>
        @error('kategori_id')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-secondary btn-block mt-3" type="submit">Edit?</button>
      </form>
    </div>
  </div>
@endsection
