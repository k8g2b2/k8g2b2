@extends('layouts.master')

@section('title', 'Masuk @ ')

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <form action="{{ route('login') }}" method="post">
        @csrf

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Email" name="email">
        </div>
        @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <input type="password" class="form-control" placeholder="Password" name="password">
        </div>
        @error('password')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-secondary btn-block mt-3" type="submit">Masuk</button>
      </form>
    </div>
  </div>
@endsection
