@extends('layouts.master')

@section('title', 'Mendaftar @ ')

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <form action="{{ route('register') }}" method="post">
        @csrf

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Nama" name="name">
        </div>
        @error('name')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Email" name="email">
        </div>
        @error('email')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <input type="password" class="form-control" placeholder="Password" name="password">
        </div>
        @error('password')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <input type="password" class="form-control" placeholder="Password:re" name="password_confirmation">
        </div>

        <div class="form-group">
          <input type="number" class="form-control" placeholder="Umur" name="umur">
        </div>
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <textarea class="form-control" placeholder="Biodata" name="biodata"></textarea>
        </div>
        @error('biodata')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <textarea class="form-control" placeholder="Alamat" name="alamat"></textarea>
        </div>
        @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-secondary btn-block mt-3" type="submit">Mendaftar</button>
      </form>
    </div>
  </div>
@endsection
