<!doctype html>
<html lang="en" class="h-100">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <!-- Berusaha untuk murni Bootstrap 😔 -->
    <style>
      /* Atur BS pl-2 & pr-2 untuk ganjil genap */

      .da-bowl .col-6:nth-child(odd) {
        padding-right: 0.5rem !important;
      }

      .da-bowl .col-6:nth-child(even) {
        padding-left: 0.5rem !important;
      }

      /* Nav aktif */
      .nav-pills .nav-link.active:not(.k8g2b2) {
        color: #6c757d !important;
      }
    </style>

    @stack('style')

    <title>@yield('title')k8g2b2</title>
  </head>
  <body class="h-100 bg-secondary">
    <div class="container h-100">
      <div class="row h-100 justify-content-center">
        <div class="col-md-9 col-lg-6">

          <!-- Navbar -->
          @include('includes.navbar')
          <!-- Navbar selesai -->

          <!-- Main -->
          <div class="row justify-content-center">
            <div class="col">

              @yield('content')

            </div>
          </div>
          <!-- Main selesai -->

          <!-- Footer -->
          <div class="row justify-content-center">
            <div class="col">
              <nav class="nav nav-pills flex-column text-center">
                <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill" href="https://gitlab.com/k8g2b2">
                  <small class="text-muted">© 2022 Kelompok 8, Grup 2, Batch 2</small>
                </a>
              </nav>
            </div>
          </div>
          <!-- Footer selesai -->

        </div>
      </div>
    </div>

    @include('sweetalert::alert')

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
    -->

    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
    <script>
      $(document).ready(function () {
        bsCustomFileInput.init()
      })
    </script>

    @stack('script')

  </body>
</html>
