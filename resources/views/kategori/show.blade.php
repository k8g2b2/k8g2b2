@extends('layouts.master')

@section('title')
  #{{ $kategori->nama }} @
@endsection

@section('content')
  <div class="row text-center da-bowl">
    <div class="col-6">
      <div class="card mb-3">
        <div class="card-body">
          <h3 class="m-0">
            <a href="/kategori/{{ $kategori->id }}" class="text-reset text-decoration-none">
              #{{ $kategori->nama }}
            </a>
          </h3>

          @auth
            <div class="btn-group mx-auto mt-2">
              <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="/kategori/{{ $kategori->id }}/edit">Edit</a>

                @if ($kategori->pertanyaan->isEmpty())
                  <a class="dropdown-item" href="/kategori/{{ $kategori->id }}"
                      onclick="event.preventDefault();
                          document.getElementById('hapus-form-{{ $kategori->id }}').submit();">Hapus</a>
                  <form id="hapus-form-{{ $kategori->id }}" action="/kategori/{{ $kategori->id }}" method="post" style="display: none;">
                    @csrf
                    @method('delete')
                  </form>
                @else
                  <a class="dropdown-item disabled">Hapus ({{ $kategori->pertanyaan->count() }})</a>
                @endif

              </div>
            </div>
          @endauth

        </div>
      </div>
    </div>
  </div>

  @forelse ($kategori->pertanyaan->reverse() as $item)
    <div class="card mb-3">
      <div class="card-body">
        {{-- <a href="/kategori/{{$item->kategori->id}}" class="badge badge-pill badge-secondary mb-2">
          #{{$item->kategori->nama}}
        </a> --}}
        <p class="mb-2">
          <b>
            {{$item->user->name}}
          </b>
        </p>
        <h3>
          <a href="/pertanyaan/{{$item->id}}" class="text-reset text-decoration-none">
            {{-- {{$item->teks}} --}}
            <?= nl2br($item->teks) ?>
          </a>
        </h3>

        @if ($item->foto != '')
          <a href="/pertanyaan/{{$item->id}}">
            <div class="mb-2">
              {{-- <img src="{{asset('foto/' . $item->foto)}}" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary"> --}}
              <img src="{{asset('foto/' . $item->foto)}}" alt="" class="img-fluid rounded w-100 mt-1 border border-secondary" width="1px" height="1px" loading="lazy">
            </div>
          </a>
        @endif

        <a href="/pertanyaan/{{$item->id}}" class="text-muted text-decoration-none d-block float-left mt-1">
          @if ($item->jawaban->count() != 0)
            {{ $item->jawaban->count() }} respons~
          @else
            Belum ada respons …
          @endif
        </a>

        @auth
          @if (Auth::id() == $item->user->id)
            <div class="btn-group float-right mt-1">
              <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="/pertanyaan/{{$item->id}}/edit">Edit</a>
                <a class="dropdown-item" href="/pertanyaan/{{ $item->id }}"
                    onclick="event.preventDefault();
                        document.getElementById('hapus-form-{{ $item->id }}').submit();">Hapus</a>
                <form id="hapus-form-{{ $item->id }}" action="/pertanyaan/{{ $item->id }}" method="post" style="display: none;">
                  @csrf
                  @method('delete')
                </form>
              </div>
            </div>
          @endif
        @endauth

      </div>
    </div>
  @empty
    <div class="card mb-3">
      <div class="card-body">
        <h3>
          <a href="/pertanyaan/create" class="text-reset text-decoration-none">
            #{{ $kategori->nama }}? …
          </a>
        </h3>
      </div>
    </div>

    <div class="row text-center da-bowl">
      <div class="col-6">
        <div class="card mb-3 rounded-pill">
          <div class="card-body">
            <h3 class="m-0">
              <a href="/pertanyaan/create" class="text-reset text-decoration-none">
                Tanya?
              </a>
            </h3>
          </div>
        </div>
      </div>
    </div>
  @endforelse
@endsection
