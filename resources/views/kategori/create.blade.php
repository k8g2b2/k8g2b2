@extends('layouts.master')

@push('style')
  <link rel="stylesheet" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css">
@endpush

@push('script')
  <script src="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.js"></script>

  <script>
    $(document).ready( function () {
      $('table.data-table').DataTable({
        'paging': false,
        // 'ordering': false,
        'info': false,
        'searching': false,
        'order': [[1, 'desc']],
      });
    } );
  </script>
@endpush

@section('title', 'Tambah #Tagar @ ')

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <form action="/kategori" method="post">
        @csrf

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Nama #" name="nama">
        </div>
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-secondary btn-block mt-3" type="submit">Tambah #</button>
      </form>
    </div>
  </div>

  @if (!$kategori->isEmpty())
    <div class="card mb-3">
      <div class="card-body">
        <table class="table table-bordered table-striped data-table rounded">
          <thead>
            <tr>
              <th>#Tagar</th>
              <th>?</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($kategori as $item)
              <tr>
                <td>#{{ $item->nama }}</td>
                <td>{{ $item->pertanyaan->count() }}</td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  @endif
@endsection
