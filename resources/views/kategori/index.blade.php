@extends('layouts.master')

@section('title', '#Tagar @ ')

@section('content')
  @if ($kategori->isEmpty())
  <div class="card mb-3">
    <div class="card-body">
      <h3>
        <a href="/kategori/create" class="text-reset text-decoration-none">
          #? …
        </a>
      </h3>
    </div>
  </div>
  @endif

  <div class="row text-center da-bowl">
    @foreach ($kategori as $key => $item)
      <div class="col-6">
        <div class="card mb-3">
          <div class="card-body">
            <h3 class="m-0">
              <a href="/kategori/{{ $item->id }}" class="text-reset text-decoration-none">
                #{{ $item->nama }}
              </a>
            </h3>

            @auth
              <div class="btn-group mx-auto mt-2">
                <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false" data-offset="12,10">•••</button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item" href="/kategori/{{ $item->id }}/edit">Edit</a>

                  @if ($item->pertanyaan->isEmpty())
                    <a class="dropdown-item" href="/kategori/{{ $item->id }}"
                        onclick="event.preventDefault();
                            document.getElementById('hapus-form-{{ $item->id }}').submit();">Hapus</a>
                    <form id="hapus-form-{{ $item->id }}" action="/kategori/{{ $item->id }}" method="post" style="display: none;">
                      @csrf
                      @method('delete')
                    </form>
                  @else
                    <a class="dropdown-item disabled">Hapus ({{ $item->pertanyaan->count() }})</a>
                  @endif

                </div>
              </div>
            @endauth

          </div>
        </div>
      </div>
    @endforeach

    <div class="col-6">
      <div class="card mb-3 rounded-pill">
        <div class="card-body">
          <h3 class="m-0">
            <a href="/kategori/create" class="text-reset text-decoration-none">
              Tambah #
            </a>
          </h3>
        </div>
      </div>
    </div>
  </div>
@endsection
