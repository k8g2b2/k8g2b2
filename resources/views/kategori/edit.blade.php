@extends('layouts.master')

@section('title')
  Edit #{{ $kategori->nama }} @
@endsection

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <form action="/kategori/{{ $kategori->id }}" method="post">
        @csrf
        @method('put')

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{ $kategori->nama }}">
        </div>
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-secondary btn-block mt-3" type="submit">Edit #</button>
      </form>
    </div>
  </div>
@endsection
