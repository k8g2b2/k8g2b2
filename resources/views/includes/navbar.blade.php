<div class="row justify-content-center">
  <div class="col mt-3">
    <nav class="nav nav-pills flex-column text-center">
      <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill k8g2b2" href="/"><b>k8g2b2</b></a>

      @auth
        <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill {{ Request::is('/') ? 'active' : '' }}" href="/">Beranda</a>
      @endauth

      @auth
        <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill {{ Request::is('profile') ? 'active' : '' }}" href="/profile">{{ Str::limit(Auth::user()->name, 10, $end=' …') }} ({{ Auth::user()->profile->umur }})</a>
      @endauth

      @auth
        <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill {{ Request::is('pertanyaan/create') ? 'active' : '' }}" href="/pertanyaan/create"><b>Tanya?</b></a>
      @endauth

      <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill {{ Request::is('kategori') ? 'active' : '' }}" href="/kategori">#Tagar</a>

      @guest
        <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill {{ Request::is('register') ? 'active' : '' }}" href="/register">Mendaftar</a>
        <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill {{ Request::is('login') ? 'active' : '' }}" href="/login">Masuk</a>
      @endguest
      @auth
        <a class="text-sm-center nav-link mb-3 bg-white text-reset rounded-pill"
            href="{{ route('logout') }}"
                onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
          Keluar(!)
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
          @csrf
        </form>
      @endauth
    </nav>
  </div>
</div>
