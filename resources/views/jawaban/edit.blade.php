@extends('layouts.master')

@push('script')
  <script src="https://cdn.tiny.cloud/1/vijhpxy7k856u1i7orvl3k7vi1247uhaq38pvw0gaaue114w/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

  <script>
    tinymce.init({
      selector: 'textarea.tinymce',
      menubar: 'format',
      forced_root_block : false,
      plugins: 'autolink',
      default_link_target: '_blank',
    });
  </script>
@endpush

@section('title', 'Edit Jawaban~ @ ')

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <form action="/jawaban/{{ $jawaban->id }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="form-group">
          {{-- <textarea name="teks" class="form-control" placeholder="Jawaban">{{ $jawaban->teks }}</textarea> --}}
          <textarea name="teks" class="form-control tinymce" placeholder="Jawaban">{!! $jawaban->teks !!}</textarea>
        </div>
        @error('teks')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <div class="custom-file">
            <input type="file" name="foto" class="custom-file-input" id="customFile">
            <label class="custom-file-label" for="customFile">Foto?</label>
          </div>
        </div>
        @error('foto')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-secondary btn-block mt-3" type="submit">Edit~</button>
      </form>
    </div>
  </div>
@endsection
