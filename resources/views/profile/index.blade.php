@extends('layouts.master')

@section('title')
  {{ $profile->user->name }} @
@endsection

@section('content')
  <div class="card mb-3">
    <div class="card-body">
      <form action="/profile/{{ $profile->id }}" method="post">
        @csrf
        @method('put')

        <div class="form-group">
          <input type="text" class="form-control" placeholder="Nama" value="{{ $profile->user->name }}" disabled>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Email" value="{{ $profile->user->email }}" disabled>
        </div>

        <div class="form-group">
          <input type="number" class="form-control" placeholder="Umur" name="umur" value="{{ $profile->umur }}">
        </div>
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <textarea class="form-control" placeholder="Biodata" name="biodata">{{ $profile->biodata }}</textarea>
        </div>
        @error('biodata')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
          <textarea class="form-control" placeholder="Alamat" name="alamat">{{ $profile->alamat }}</textarea>
        </div>
        @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button class="btn btn-secondary btn-block mt-3" type="submit">Edit</button>
      </form>
    </div>
  </div>
@endsection
